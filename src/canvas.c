/* Psyfidot
 * Copyright (C) 2023 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "common.h"
#include "canvas.h"
#include <stdio.h>
#include <stdlib.h>

// The only reason I include SDL is so I can make use of SDL_IntersectRect
// Other than that, this file should be completely independent from the frontend library
#include <SDL2/SDL.h>

// Selection specific methods
void canvas_get_selections_intersection(canvas_t *canvas, SDL_Rect *result)
{
    SDL_Rect first_rect = {
        canvas->selection.top_left % canvas->width, canvas->selection.top_left / canvas->width,
        canvas->selection.width, canvas->selection.height
    };
    SDL_Rect second_rect = {
        canvas->shape_selection.top_left % canvas->width, canvas->shape_selection.top_left / canvas->width,
        canvas->shape_selection.width, canvas->shape_selection.height
    };

    SDL_IntersectRect(&first_rect, &second_rect, result);
}

void canvas_update_selection_boundaries(canvas_t *canvas, selection_t *selection)
{
    selection->width = abs(selection->start % canvas->width - selection->end % canvas->width) + 1;
    selection->height = abs(selection->start / canvas->width - selection->end / canvas->width) + 1;

    selection->top_left = MIN(selection->start % canvas->width, selection->end % canvas->width)
        + MIN(selection->start / canvas->width, selection->end / canvas->width) * canvas->width;
}

void canvas_create_pixel_state(canvas_t *canvas, pixel_state_t *state)
{
    state->pixels = malloc(sizeof(canvas_color_t) * canvas->width * canvas->height);

    // We want the modified buffer to be filled with zero's because most of the time nothing will be copied to it
    state->was_modified_in_this_state = calloc(canvas->width * canvas->height, sizeof(bool));
}

void pixel_state_free(pixel_state_t *state)
{
    free(state->pixels);
    free(state->was_modified_in_this_state);
}

void canvas_load_palette(canvas_t *canvas, const char *palette_path)
{
    FILE *file = fopen(palette_path, "r");

    if (!file)
    {
        abort_with_message("Failed to open palette file %s", palette_path);
    }
    
    // Collecting the total number of lines in the file (i.e. total_colors)
    size_t total_lines = count_lines_in_file(file);

    // Allocating enough space for the palette color array
    canvas->palette = malloc(sizeof(canvas_color_t) * total_lines);
    
    for (int i = 0; i < total_lines; i++)
    {
        if (fscanf(file, "%hhu %hhu %hhu\n",
                   &canvas->palette[i].r,
                   &canvas->palette[i].g,
                   &canvas->palette[i].b) != 3)
        {
            fclose(file);
            abort_with_message("Failed to parse palette file %s! Error at line %d", palette_path, i + 1);
        }
    }

    canvas->palette_length = total_lines;
    fclose(file);
}

void canvas_start_with_new_file(canvas_t *canvas, char *file_path, int width, int height)
{
    canvas->width = width;
    canvas->height = height;
    canvas->file_path = strdup(file_path);
    canvas->history_length = 0;
    
    canvas_create_new_history_entry(canvas);
    memset(canvas->history[0].pixels, 0, sizeof(canvas_color_t) * width * height);
}

void canvas_clear_selection(canvas_t *canvas)
{
    canvas->selection.width = canvas->width;
    canvas->selection.height = canvas->height;

    canvas->selection.top_left = 0;
    canvas->selection.start = canvas->selection.end = 0;
}

// Just sets some default values after the image and the palette have been loaded
void canvas_finish_initialization(canvas_t *canvas)
{
    canvas->active_brush = BRUSH_REGULAR;
    canvas->active_modes = 0;

    queue_create(&canvas->bucket_queue, canvas->width * canvas->height);
    queue_create(&canvas->undrawn_modifications, canvas->width * canvas->height);

    canvas_set_active_color(canvas, &canvas->palette[0]);

    // Making the canvas selection occupy the whole screen by default
    canvas_clear_selection(canvas);
}

bool canvas_is_index_inside_selection(canvas_t *canvas, size_t index)
{
    size_t y = index / canvas->width;
    size_t x = index % canvas->width;

    size_t top_left_y = canvas->selection.top_left / canvas->width;
    size_t top_left_x = canvas->selection.top_left % canvas->width;
    
    return y >= top_left_y && y <= top_left_y + (canvas->selection.height - 1)
        && x >= top_left_x && x <= top_left_x + (canvas->selection.width - 1);
}

// Inserts a new history entry at the first item of the history list
// It will initially have the same data with the older history item
void canvas_create_new_history_entry(canvas_t *canvas)
{
    // Move all elements to the right and insert at position zero
    // If an element exists at the last position, it should thus be deleted first
    if (canvas->history_length == MAX_HISTORY_LENGTH)
    {
        pixel_state_free(&canvas->history[MAX_HISTORY_LENGTH - 1]);
    }
    else
    {
        canvas->history_length++;
    }

    memmove(canvas->history + 1, canvas->history, sizeof(pixel_state_t) * (MAX_HISTORY_LENGTH - 1));
    canvas_create_pixel_state(canvas, &canvas->history[0]);

    if (canvas->history_length != 1)
    {
        memcpy(canvas->history[0].pixels, canvas->history[1].pixels, sizeof(canvas_color_t) * canvas->width * canvas->height);
    }
}

void canvas_delete_latest_history_entry(canvas_t * canvas)
{
    // There need to be atleast two history records, the current state and an older one
    if (canvas->history_length == 1) return;

    // Move all other states to the left and delete the old reference to the history's first state (previous active state)
    pixel_state_free(&canvas->history[0]);

    memmove(canvas->history, canvas->history + 1, sizeof(pixel_state_t) * (MAX_HISTORY_LENGTH - 1));
    canvas->history_length--;
}

void canvas_load_png_with_preferred_size(canvas_t *canvas, const char *path, int width, int height)
{
    FILE *file = fopen(path, "rb");
    canvas->file_path = strdup(path);
    
    if (!file)
    {
        fclose(file);
        abort_with_message("Failed to read PNG file from %s", path);
    }

    // Check if the file followes the png format by viewing its signature (first 8 bytes)
    char header[8];
    fread(header, 1, 8, file);

    if (png_sig_cmp(header, 0, 8))
    {
        fclose(file);
        abort_with_message("Failed to recognize file %s as PNG", path);
    }

    // Allocating and initializing these two important structures
    // Using the default error and warning handlers
    png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    png_infop info_ptr = png_create_info_struct(png_ptr);

    if (!png_ptr || !info_ptr)
    {
        fclose(file);
        abort_with_message("Failed to initialize PNG components for file");
    }
    
    // Reading PNG data from the input file
    // Also letting libpng know that we've already read the signature
    png_init_io(png_ptr, file);
    png_set_sig_bytes(png_ptr, 8);

    // Reading PNG file info and storing some info to our canvas struct
    png_read_info(png_ptr, info_ptr);

    if (png_get_color_type(png_ptr, info_ptr) != PNG_COLOR_TYPE_RGBA)
    {
        abort_with_message("PNG file %s is not of type RGBA!", path);
    }

    canvas->history_length = 0;
    size_t png_width = png_get_image_width(png_ptr, info_ptr);
    size_t png_height = png_get_image_height(png_ptr, info_ptr);

    canvas->width = width > 0 ? width : png_width;
    canvas->height = height > 0 ? height : png_height;
    
    // Reading actual pixel data
    png_read_update_info(png_ptr, info_ptr);

    // Allocating enough space and copying the file's pixel data
    png_bytep* row_pointers = malloc(sizeof(png_bytep) * png_height);
    canvas_create_new_history_entry(canvas);
    
    size_t bytes_per_row = png_get_rowbytes(png_ptr, info_ptr);
    
    for (int i = 0; i < png_height; i++)
    {
        row_pointers[i] = malloc(bytes_per_row);
    }
    
    png_read_image(png_ptr, row_pointers);

    for (int i = 0; i < MIN(png_height, canvas->height); i++)
    {
        for (int j = 0; j < MIN(png_width, canvas->width); j++)
        {
            // Copying each pixel into the canvas pixel array
            // This is mandatory to support multiply formats
            // Also, we're ignoring the alpha channel
            memcpy(&canvas->history[0].pixels[i * canvas->width + j], &row_pointers[i][j * 4], 3);
        }

        free(row_pointers[i]);
    }

    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
    free(row_pointers);
    fclose(file);
}

// For now, this function will assume that the user wants to export it to a .PNG
// In the future, an EXPORT_OPTION enum could be introduced
void canvas_save_progress(canvas_t *canvas)
{
    FILE *file = fopen(canvas->file_path, "wb");

    if (!file)
        abort_with_message("Failed to open file %s while saving", canvas->file_path);

    // Creating the libpng structs
    png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    png_infop info_ptr = png_create_info_struct(png_ptr);

    // Setting image attributes
    png_set_IHDR(png_ptr, info_ptr, canvas->width, canvas->height, 8,
                 PNG_COLOR_TYPE_RGBA, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

    // Saving canvas pixel data to our png file
    png_byte **row_pointers = png_malloc(png_ptr, canvas->height * sizeof(png_byte*));

    for (int i = 0; i < canvas->height; i++)
    {
        // We will export using transparency for eraser_color, hence "4" pixel values
        row_pointers[i] = png_malloc(png_ptr, sizeof(uint8_t) * canvas->width * 4);

        for (int j = 0; j < canvas->width; j++)
        {
            size_t index = i * canvas->width + j;
            memcpy(&row_pointers[i][j * 4], &canvas->history[0].pixels[index], sizeof(canvas_color_t));
            
            // Making it visible if it's not eraser_color, otherwise totally invisible
            row_pointers[i][j * 4 + 3] = is_pixel_empty(&canvas->history[0].pixels[index]) ? 0 : 255;
        }
    }

    // Attaching file into png writer
    png_init_io(png_ptr, file);
    png_set_rows(png_ptr, info_ptr, row_pointers);
    png_write_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);

    png_destroy_write_struct(&png_ptr, &info_ptr);
    free(row_pointers);
    
    fclose(file);
}

// Marks the pixel as modified for the next rendering cycle
// While canvas->history[0].was_modified_in_this_state is useful primarly during history operations,
// the undrawn_modifications queue clears right after the frame is over, so it's even more specific.
// The queue is frame-specific and usually smaller, thus more performant
void canvas_mark_pixel_as_modified(canvas_t *canvas, size_t i)
{
    queue_append(&canvas->undrawn_modifications, i);
    canvas->history[0].was_modified_in_this_state[i] = true;
}

void canvas_set_pixel_color(canvas_t *canvas, size_t index, canvas_color_t *color)
{
    canvas_mark_pixel_as_modified(canvas, index);
    memcpy(&canvas->history[0].pixels[index], color, sizeof(canvas_color_t));
}

void canvas_set_active_color(canvas_t *canvas, canvas_color_t *color)
{
    memcpy(&canvas->active_color, color, sizeof(canvas_color_t));
}

bool canvas_is_mode_active(canvas_t *canvas, mode_e mode)
{
    return canvas->active_modes & mode;
}

bool is_pixel_of_color(canvas_color_t *pixel, canvas_color_t *color)
{
    return pixel->r == color->r && pixel->g == color->g && pixel->b == color->b;
}

bool is_pixel_empty(canvas_color_t *pixel)
{
    return !pixel->r && !pixel->g && !pixel->b;
}

void canvas_change_pixel_lighting(canvas_t *canvas, size_t index, int change)
{
    canvas_color_t *pixel = &canvas->history[0].pixels[index];

    // Ignore the action if the pixel is already erased
    if (is_pixel_empty(&canvas->history[0].pixels[index])) return;

    pixel->r = clamp(0, 255, pixel->r + change);
    pixel->g = clamp(0, 255, pixel->g + change);
    pixel->b = clamp(0, 255, pixel->b + change);

    canvas_mark_pixel_as_modified(canvas, index);
}

// Returns the symmetrical index upon the horizontal axis
// These two functions work with the canvas selection, so mirroring can get local
size_t canvas_get_sym_horizontal_index(canvas_t *canvas, size_t index)
{
    return 2 * (canvas->selection.top_left / canvas->width) * canvas->width + index % canvas->width + ((canvas->selection.height - 1) - (index / canvas->width)) * canvas->width;
}

size_t canvas_get_sym_vertical_index(canvas_t *canvas, size_t index)
{
    return 2 * (canvas->selection.top_left % canvas->width) + index / canvas->width * canvas->width + (canvas->selection.width - 1) - (index % canvas->width);
}

// Effect implementations
bool canvas_try_add_to_bucket_queue(canvas_t *canvas, size_t index, canvas_color_t *color_to_replace)
{
    // Using the latest history record, otherwise the bucket tool will be confused
    // Using canvas->history[0].was_modified_in_this_state to determine if the bucket has already reached said pixel
    if (is_pixel_of_color(&canvas->history[1].pixels[index], color_to_replace) && !canvas->history[0].was_modified_in_this_state[index])
    {
        queue_append(&canvas->bucket_queue, index);
        canvas->history[0].was_modified_in_this_state[index] = true;
    }
}

void canvas_apply_bucket(canvas_t *canvas, size_t index)
{
    canvas_color_t *color_to_replace = &canvas->history[1].pixels[index];

    // If the user is trying to fill over with the same color, just ignore him
    if (!canvas_is_index_inside_selection(canvas, index) || is_pixel_of_color(&canvas->active_color, color_to_replace))
    {
        return;
    }
    
    // Re-implemented this without using recursion because the function failed to run for larger files (maximum recursion depth)
    queue_clear(&canvas->bucket_queue);
    canvas_try_add_to_bucket_queue(canvas, index, color_to_replace);

    size_t start_y = canvas->selection.top_left / canvas->width;
    size_t start_x = canvas->selection.top_left % canvas->width;

    while (!queue_is_empty(&canvas->bucket_queue))
    {
        size_t index = queue_pop(&canvas->bucket_queue);
        canvas_apply_brush(canvas, BRUSH_REGULAR, index);

        if (index % canvas->width != start_x) canvas_try_add_to_bucket_queue(canvas, index - 1, color_to_replace);
        if (index % canvas->width != start_x + canvas->selection.width - 1) canvas_try_add_to_bucket_queue(canvas, index + 1, color_to_replace);
        if (index / canvas->width != start_y) canvas_try_add_to_bucket_queue(canvas, index - canvas->width, color_to_replace);
        if (index / canvas->width != start_y + canvas->selection.height - 1) canvas_try_add_to_bucket_queue(canvas, index + canvas->width, color_to_replace);
    }
}

void canvas_apply_outline(canvas_t *canvas, size_t index)
{
    // Some effects, including the outline effect, require a reference to the old version of the canvas to work
    canvas_color_t *old_pixels = canvas->history[1].pixels;

    size_t start_y = canvas->selection.top_left / canvas->width;
    size_t start_x = canvas->selection.top_left % canvas->width;

    for (int y = start_y; y < start_y + canvas->selection.height; y++)
    {
        for (int x = start_x; x < start_x + canvas->selection.width; x++)
        {
            size_t i = x + y * canvas->width;

            // Check if the pixel should be turned into an outline
            if ((
                    (x != start_x && !is_pixel_empty(&old_pixels[i - 1])) ||
                    (x < start_x + canvas->selection.width - 1 && !is_pixel_empty(&old_pixels[i + 1])) ||
                    (y > start_y && !is_pixel_empty(&old_pixels[i - canvas->width])) ||
                    (y < start_y + canvas->selection.height - 1 && !is_pixel_empty(&old_pixels[i + canvas->width]))
                    ) && is_pixel_empty(&canvas->history[0].pixels[i]))
            {
                canvas_set_pixel_color(canvas, i, &canvas->active_color);
            }
        }
    }
}

void canvas_apply_color_inversion(canvas_t *canvas, size_t index)
{
    size_t start_y = canvas->selection.top_left / canvas->width;
    size_t start_x = canvas->selection.top_left % canvas->width;
    
    for (int y = start_y; y < start_y + canvas->selection.height; y++)
    {
        for (int x = start_x; x < start_x + canvas->selection.width; x++)
        {
            size_t i = x + y * canvas->width;
            
            if (is_pixel_empty(&canvas->history[0].pixels[i])) continue;
        
            canvas->history[0].pixels[i].r = 255 - canvas->history[0].pixels[i].r;
            canvas->history[0].pixels[i].g = 255 - canvas->history[0].pixels[i].g;
            canvas->history[0].pixels[i].b = 255 - canvas->history[0].pixels[i].b;
            canvas_mark_pixel_as_modified(canvas, i);
        }
    }
}

void canvas_apply_eye_dropper(canvas_t *canvas, size_t index)
{
    // Just copies the color under the cursor to active_color
    canvas_set_active_color(canvas, &canvas->history[0].pixels[index]);
}

void canvas_apply_kill(canvas_t *canvas, size_t index)
{
    size_t start = canvas->selection.top_left;
    
    // Just erasing the selected region
    for (int i = 0; i < canvas->selection.height; i++)
    {
        memset(&canvas->history[0].pixels[start + i * canvas->width], 0, sizeof(canvas_color_t) * canvas->selection.width);
        memset(&canvas->history[0].was_modified_in_this_state[start + i * canvas->width], true, sizeof(bool) * canvas->selection.width);

        // For performance's shake, we won't be appending any of the modified pixels to the rendering queue
        // The frontend will simply draw a rectangle on top of the affected area
    }
}

/*
 * All movement effects will work with selected regions
 * When pixels exceed their boundaries, they will be just erased
 */
void canvas_apply_move_left(canvas_t *canvas, size_t index)
{
    canvas_color_t *old_pixels = canvas->history[1].pixels;
    
    for (int y = 0; y < canvas->selection.height; y++)
    {
        for (int x = 0; x < canvas->selection.width; x++)
        {
            size_t i = canvas->selection.top_left + x + y * canvas->width;

            // The first column shall be erased
            if (x == canvas->selection.width - 1)
                canvas_apply_eraser(canvas, i);
            else
                canvas_set_pixel_color(canvas, i, &old_pixels[i + 1]);
        }
    }
}

void canvas_apply_move_right(canvas_t *canvas, size_t index)
{
    canvas_color_t *old_pixels = canvas->history[1].pixels;
    
    for (int y = 0; y < canvas->selection.height; y++)
    {
        for (int x = 0; x < canvas->selection.width; x++)
        {
            size_t i = canvas->selection.top_left + x + y * canvas->width;

            // The first column shall be erased
            if (x == 0)
                canvas_apply_eraser(canvas, i);
            else
                canvas_set_pixel_color(canvas, i, &old_pixels[i - 1]);
        }
    }
}

void canvas_apply_move_up(canvas_t *canvas, size_t index)
{
    for (int y = 0; y < canvas->selection.height - 1; y++)
    {
        for (int x = 0; x < canvas->selection.width; x++)
        {
            size_t i = canvas->selection.top_left + x + y * canvas->width;
            canvas_set_pixel_color(canvas, i, &canvas->history[0].pixels[i + canvas->width]);
        }
    }

    // Erasing the bottom row
    for (int i = 0; i < canvas->selection.width; i++)
    {
        canvas_apply_eraser(canvas, canvas->selection.top_left + (canvas->selection.height - 1) * canvas->width + i);
    }
}

void canvas_apply_move_down(canvas_t *canvas, size_t index)
{
    for (int y = canvas->selection.height - 1; y > 0; y--)
    {
        for (int x = 0; x < canvas->selection.width; x++)
        {
            size_t i = canvas->selection.top_left + x + y * canvas->width;
            canvas_set_pixel_color(canvas, i, &canvas->history[0].pixels[i - canvas->width]);
        }
    }

    // Erasing the top row
    for (int i = 0; i < canvas->selection.width; i++)
    {
        canvas_apply_eraser(canvas, canvas->selection.top_left + i);
    }
}

// The rectangle effect will draw a rectangle inside the current canvas selection
void canvas_apply_fill_rect(canvas_t *canvas, size_t index)
{
    if (!canvas->shape_selection.width) return;

    SDL_Rect intersection;
    canvas_get_selections_intersection(canvas, &intersection);
    
    for (int i = intersection.y; i < intersection.y + intersection.h; i++)
    {
        for (int j = intersection.x; j < intersection.x + intersection.w; j++)
        {
            canvas_apply_brush(canvas, BRUSH_REGULAR, j + i * canvas->width);
        }
    }
}

void canvas_apply_outline_rect(canvas_t *canvas, size_t index)
{
    if (!canvas->shape_selection.width) return;

    size_t top_left_x = canvas->shape_selection.top_left % canvas->width;
    size_t top_left_y = canvas->shape_selection.top_left / canvas->width;

    // Draw each side without the inside
    for (int i = top_left_x + 1; i < top_left_x + canvas->shape_selection.width - 1; i++)
    {
        size_t top = top_left_y * canvas->width + i;
        size_t bottom = (top_left_y + canvas->shape_selection.height - 1) * canvas->width + i;
        
        if (canvas_is_index_inside_selection(canvas, top))
            canvas_apply_brush(canvas, BRUSH_REGULAR, top);

        if (canvas_is_index_inside_selection(canvas, bottom))
            canvas_apply_brush(canvas, BRUSH_REGULAR, bottom);
    }

    for (int i = top_left_y; i < top_left_y + canvas->shape_selection.height; i++)
    {
        size_t left = i * canvas->width + top_left_x;
        size_t right = i * canvas->width + top_left_x + canvas->shape_selection.width - 1;

        if (canvas_is_index_inside_selection(canvas, left))
            canvas_apply_brush(canvas, BRUSH_REGULAR, left);

        if (canvas_is_index_inside_selection(canvas, right))
            canvas_apply_brush(canvas, BRUSH_REGULAR, right);
    }
}

void canvas_apply_yank(canvas_t *canvas, size_t index)
{
    // Copying the canvas region content onto the clipboard
    if (canvas->clipboard_width != 0)
        free(canvas->clipboard);

    canvas->clipboard_width = canvas->selection.width;
    canvas->clipboard_height = canvas->selection.height;
    canvas->clipboard = malloc(canvas->clipboard_width * canvas->clipboard_height * sizeof(canvas_color_t));

    for (int i = 0; i < canvas->clipboard_height; i++)
    {
        memcpy(&canvas->clipboard[i * canvas->clipboard_width],
               &canvas->history[0].pixels[canvas->selection.top_left + i * canvas->width],
               canvas->clipboard_width * sizeof(canvas_color_t));
    }

    // When a yank is made, the selection is automatically cleared
    canvas_clear_selection(canvas);
}

void canvas_apply_paste(canvas_t *canvas, size_t index)
{
    size_t width_used = MIN(canvas->width - (index % canvas->width), canvas->clipboard_width);
    size_t height_used = MIN(canvas->height - (index / canvas->width), canvas->clipboard_height);

    // Pasting the clipboard contents at the target index and rightwards-downwards
    for (int i = 0; i < height_used; i++)
    {
        for (int j = 0; j < width_used; j++)
        {
            canvas_set_pixel_color(canvas, index + i * canvas->width + j, &canvas->clipboard[i * canvas->clipboard_width + j]);
        }
    }
}

void canvas_apply_replace_color(canvas_t *canvas, size_t index)
{
    canvas_color_t color_to_replace = canvas->history[0].pixels[index];

    size_t region_x = canvas->selection.top_left % canvas->width;
    size_t region_y = canvas->selection.top_left / canvas->width;

    // Just looping through the selected region and replacing all instances with the active color
    for (int y = region_y; y < region_y + canvas->selection.height; y++)
    {
        for (int x = region_x; x < region_x + canvas->selection.width; x++)
        {
            size_t i = x + y * canvas->width;

            if (is_pixel_of_color(&canvas->history[0].pixels[i], &color_to_replace))
                canvas_apply_regular(canvas, i);
        }
    }
}

// Brush implementations
void canvas_apply_brush(canvas_t *canvas, brush_e brush_type, size_t i)
{
    brush_handler handler = brush_handlers[(int) brush_type];

    handler(canvas, i);
    
    // Modifying behaviour based on activated modes
    if (canvas_is_mode_active(canvas, MODE_VERTICAL_MIRROR))
    {
        handler(canvas, canvas_get_sym_vertical_index(canvas, i));
    }
    
    if (canvas_is_mode_active(canvas, MODE_HORIZONTAL_MIRROR))
    {
        handler(canvas, canvas_get_sym_horizontal_index(canvas, i));
        
        // If both of them are active, apply the effect to an extra index as well
        if (canvas_is_mode_active(canvas, MODE_VERTICAL_MIRROR))
        {
            handler(canvas, canvas_get_sym_vertical_index(canvas, canvas_get_sym_horizontal_index(canvas, i)));
        }
    }
}

void canvas_apply_spray(canvas_t *canvas, size_t i)
{
    size_t iterations = 0;
    
    // Randomly drawing pixels around the current position
    while (iterations < SPRAY_DENSITY)
    {
        iterations++;
        
        size_t random_x = (i % canvas->width) + get_random_in_range(-SPRAY_SPREAD, SPRAY_SPREAD);
        size_t random_y = i / canvas->width + get_random_in_range(-SPRAY_SPREAD, SPRAY_SPREAD);
        
        // Check if the spray is out of the display
        if (random_x < 0 || random_x >= canvas->width || random_y < 0 || random_y >= canvas->height) continue;

        size_t index = random_x + random_y * canvas->width;
        if (!canvas_is_index_inside_selection(canvas, index)) continue;

        if (!canvas->history[0].was_modified_in_this_state[index])
        {
            canvas_set_pixel_color(canvas, random_x + random_y * canvas->width, &canvas->active_color);
        }
    }
}

void canvas_apply_regular(canvas_t *canvas, size_t i)
{
    canvas_set_pixel_color(canvas, i, &canvas->active_color);
}

void canvas_apply_eraser(canvas_t *canvas, size_t i)
{
    memset(&canvas->history[0].pixels[i], 0, sizeof(canvas_color_t));
    canvas_mark_pixel_as_modified(canvas, i);
}

void canvas_apply_darken(canvas_t *canvas, size_t i)
{
    // Just makes the current color darker by decreasing all RGB components
    canvas_change_pixel_lighting(canvas, i, -CHANGE_LIGHTING_STEP);
}

void canvas_apply_lighten(canvas_t *canvas, size_t i)
{
    canvas_change_pixel_lighting(canvas, i, CHANGE_LIGHTING_STEP);
}
