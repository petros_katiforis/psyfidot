/* Psyfidot
 * Copyright (C) 2023 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "common.h"

int clamp(int min, int max, int value)
{
    return value < min ? min : value > max ? max : value;
}

int get_random_in_range(int start, int end)
{
    return start + rand() % (end - start + 1);
}

size_t count_lines_in_file(FILE *file)
{
    // Incrementing total lines when a new line character is reached
    char current_char;
    size_t total_lines = 0;
    
    while ((current_char = fgetc(file)) != EOF)
    {
        if (current_char == '\n') total_lines++;
    }

    // Making sure to reposition the file cursor to the start
    rewind(file);
    
    return total_lines;
}

void abort_with_message(const char *format, ...)
{
    va_list arguments;
    va_start(arguments, format);

    // Writing the message to stderr followed by a new line character
    vfprintf(stderr, format, arguments);
    fprintf(stderr, "\n");

    va_end(arguments);
    exit(EXIT_FAILURE);
}
