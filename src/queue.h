/* Psyfidot
 * Copyright (C) 2023 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _QUEUE_H
#define _QUEUE_H

#include <stdbool.h>
#include <stdlib.h>

/*
 * This data structure seemed to be common among many parts of the editor, so I moved it to its own file
 * It can describe pixels waiting to be re-rendered, pixel neigbours waiting to be filled by the bucket tool and so on
 */
typedef struct
{
    size_t *data;
    size_t max;
    size_t start, end;
} queue_t;

void queue_create(queue_t *queue, size_t max);
void queue_clear(queue_t *queue);
bool queue_is_empty(queue_t *queue);

// Returns the first item of the queue
size_t queue_pop(queue_t *queue);

void queue_append(queue_t *queue, size_t value);

#endif
