/* Psyfidot
 * Copyright (C) 2023 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _CANVAS_H
#define _CANVAS_H

#include <png.h>
#include "queue.h"
#include <stdbool.h>
#include <SDL2/SDL.h>

// Some constant configuration
#define CHANGE_LIGHTING_STEP 20
#define MAX_HISTORY_LENGTH 35
#define SPRAY_DENSITY 5
#define SPRAY_SPREAD 4

// Transparent colors will not be supported because they will confuse the user when looking at the background
// The only transparent pixels will be the once that are drawn using eraser_color
typedef struct
{
    unsigned char r, g, b;
} canvas_color_t;

// Each activatable tool will be represented by an enum
typedef enum
{
    BRUSH_REGULAR,
    BRUSH_ERASER,
    BRUSH_SPRAY,
    BRUSH_DARKEN,
    BRUSH_LIGHTEN,
    TOTAL_BRUSHES
} brush_e;

// Effects will not work like brushes, because they are not interactive
// They are applied only once in each stroke and cannot be used in holding_mouse mode
typedef enum
{
    EFFECT_OUTLINE,
    EFFECT_BUCKET,
    EFFECT_COLOR_INVERSION,
    EFFECT_EYE_DROPPER,
    EFFECT_KILL,
    EFFECT_MOVE_LEFT,
    EFFECT_MOVE_RIGHT,
    EFFECT_MOVE_UP,
    EFFECT_MOVE_DOWN,
    EFFECT_FILL_RECT,
    EFFET_OUTLINE_RECT,
    EFFECT_YANK,
    EFFECT_PASTE,
    EFFECT_REPLACE_COLOR,
    TOTAL_EFFECTS
} effect_e;

// Modes manipulate how activated brushes work!
// They can be multiple of them active at the same time, so I've using flags
#define TOTAL_MODES 2
typedef enum
{
    MODE_VERTICAL_MIRROR = 1 << 0,
    MODE_HORIZONTAL_MIRROR = 1 << 1
} mode_e;

typedef struct
{
    canvas_color_t *pixels;

    // Stores whether each pixels needs to be re-rendered during state-switching and more
    // This is a massive optimization boost, especially for larger images when switching between states through the history
    bool *was_modified_in_this_state;
} pixel_state_t;

typedef struct
{
    size_t start, end;
    // Some variables about selection, they will just be calculated and stored here
    // No need to do the math every time they are being used
    size_t width, height;
    size_t top_left;
} selection_t;

// A struct containing all the useful information that our canvas can hold
typedef struct
{
    char *file_path;
    int width, height;
    
    queue_t bucket_queue;
    queue_t undrawn_modifications;
    
    brush_e active_brush;
    mode_e active_modes;

    canvas_color_t active_color;
    size_t palette_length;
    canvas_color_t *palette;

    // The canvas selection modifies mirroring behaviour and constrains brushes to its boundaries
    selection_t selection;
    // The effect selection is used to create shapes
    selection_t shape_selection;
    
    canvas_color_t *clipboard;
    size_t clipboard_width, clipboard_height;
    
    // The history will just be a list of canvas_color_t lists, that is pixel grids
    // The first item of the history array is reserved for the current canvas state
    size_t history_length;
    pixel_state_t history[MAX_HISTORY_LENGTH];
} canvas_t;

void canvas_get_selections_intersection(canvas_t *canvas, SDL_Rect *result);
void canvas_update_selection_boundaries(canvas_t *canvas, selection_t *selection);

void canvas_load_palette(canvas_t *canvas, const char *palette_path);
void canvas_load_png_with_preferred_size(canvas_t *canvas, const char *path, int width, int height);
void canvas_finish_initialization(canvas_t *canvas);
void canvas_set_active_color(canvas_t *canvas, canvas_color_t *color);
void canvas_save_progress(canvas_t *canvas);
void canvas_start_with_new_file(canvas_t *canvas, char *file_path, int width, int height);
void canvas_create_new_history_entry(canvas_t *canvas);
void canvas_delete_latest_history_entry(canvas_t *canvas);
void canvas_set_pixel_color(canvas_t *canvas, size_t index, canvas_color_t *color);
void canvas_clear_selection(canvas_t *canvas);

// Checks if the specified index is inside canvas_selection
bool canvas_is_index_inside_selection(canvas_t *canvas, size_t index);

// This functions are general-purspose and do not require a canvas struct, although they are still related to the canvas functionality
bool is_pixel_of_color(canvas_color_t *pixel, canvas_color_t *color);
bool is_pixel_empty(canvas_color_t *pixel);

bool canvas_is_mode_active(canvas_t *canvas, mode_e mode);
void canvas_change_pixel_lighting(canvas_t *canvas, size_t index, int change);

// Calculating symmetrical positions
size_t canvas_get_sym_horizontal_index(canvas_t *canvas, size_t index);
size_t canvas_get_sym_vertical_index(canvas_t *canvas, size_t index);

// Effect and tool implementations are declared here!
void canvas_try_to_fill_with_color(canvas_t *canvas, size_t i, canvas_color_t *old_color);
void canvas_apply_bucket(canvas_t *canvas, size_t index);
void canvas_apply_outline(canvas_t *canvas, size_t index);
void canvas_apply_color_inversion(canvas_t *canvas, size_t index);
void canvas_apply_eye_dropper(canvas_t *canvas, size_t index);
void canvas_apply_move_left(canvas_t *canvas, size_t index);
void canvas_apply_move_right(canvas_t *canvas, size_t index);
void canvas_apply_move_up(canvas_t *canvas, size_t index);
void canvas_apply_move_down(canvas_t *canvas, size_t index);
void canvas_apply_fill_rect(canvas_t *canvas, size_t index);
void canvas_apply_outline_rect(canvas_t *canvas, size_t index);
void canvas_apply_yank(canvas_t *canvas, size_t index);
void canvas_apply_paste(canvas_t *canvas, size_t index);
void canvas_apply_replace_color(canvas_t *canvas, size_t index);

void canvas_apply_brush(canvas_t *canvas, brush_e brush_type, size_t i);
void canvas_apply_regular(canvas_t *canvas, size_t i);
void canvas_apply_spray(canvas_t *canvas, size_t i);
void canvas_apply_eraser(canvas_t *canvas, size_t i);
void canvas_apply_darken(canvas_t *canvas, size_t i);
void canvas_apply_lighten(canvas_t *canvas, size_t i);
void canvas_apply_kill(canvas_t *canvas, size_t i);

typedef void (*brush_handler) (canvas_t*, size_t);

// A list holding all brush handlers based on their enum order
static brush_handler brush_handlers[TOTAL_BRUSHES] = {
    canvas_apply_regular,
    canvas_apply_eraser,
    canvas_apply_spray,
    canvas_apply_darken,
    canvas_apply_lighten
};

#endif
