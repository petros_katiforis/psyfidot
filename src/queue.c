/* Psyfidot
 * Copyright (C) 2023 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "queue.h"

void queue_create(queue_t *queue, size_t max)
{
    queue->data = malloc(max * sizeof(size_t));
    queue->max = max;
    
    queue_clear(queue);
}

void queue_clear(queue_t *queue)
{
    queue->start = queue->end = 0;
}

bool queue_is_empty(queue_t *queue)
{
    return queue->end - queue->start <= 0;
}

// All these methods take in consideration that the user will never try to insert more than queue->max items
// In our case it's completely safe, because the grid can only hold a limited amount of pixels
size_t queue_pop(queue_t *queue)
{
    return queue->data[queue->start++];
}

void queue_append(queue_t *queue, size_t value)
{
    queue->data[queue->end++] = value;
}
