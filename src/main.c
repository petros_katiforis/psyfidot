/* Psyfidot
 * Copyright (C) 2023 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <errno.h>
#include <unistd.h>
#include "canvas.h"
#include "common.h"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>

/*
 * This file will contain all functionality related to utilising the canvas_t struct
 * It will hold functions to create a system window, render it to the screen, handle keyboard events and so on
 * Think of it like a bridge between the data of canvas_t and the computer user
 */

#define PALETTE_PIXEL_SIZE 15

/*
 * Global SDL frontend variables
 */
SDL_Rect selection_rect;

canvas_t canvas;
SDL_Window *window;
SDL_Renderer *renderer;
SDL_Event event;

// Holds the previous focused pixel's index in the canvas list
// It's needed for both optimizations and tool functionality
size_t latest_pixel_index;

unsigned int gui_pixel_size;
size_t palette_colors_per_row;
SDL_Rect pixel;

bool is_holding_left_mouse = false;
bool drawing_while_holding_mouse = false;

struct {
    SDL_Keycode key;
    void (*effect_handler) (canvas_t *canvas, size_t index);

} effect_bindings[TOTAL_EFFECTS] = {
    {SDLK_o, canvas_apply_outline},
    {SDLK_f, canvas_apply_bucket},
    {SDLK_i, canvas_apply_color_inversion},
    {SDLK_BACKQUOTE, canvas_apply_eye_dropper},
    {SDLK_k, canvas_apply_kill},
    {SDLK_LEFT, canvas_apply_move_left},
    {SDLK_RIGHT, canvas_apply_move_right},
    {SDLK_UP, canvas_apply_move_up},
    {SDLK_DOWN, canvas_apply_move_down},
    {SDLK_r, canvas_apply_fill_rect},
    {SDLK_4, canvas_apply_outline_rect},
    {SDLK_y, canvas_apply_yank},
    {SDLK_p, canvas_apply_paste},
    {SDLK_a, canvas_apply_replace_color}
};

void abort_with_help(const char *extra_message)
{
    puts("psyfidot pixel art editor v1.3.1");
    printf("<error>: %s\n", extra_message);

    puts("<psyfidot> (palette_file: string) (png_file_name: string) [optional width: uint] [optional height: uint]\n");
    puts("    [The last two arguments are only going to be used if the specified file does not exist, or if you want to resize the canvas]");
    puts("     Please make sure that your palette files follow the .psy format (see example at repository tree)");

    exit(EXIT_FAILURE);
}

size_t window_to_pixel_index(int window_x, int window_y)
{
    // Error handling is being implemented elsewhere, right before the calls of this function
    return window_y / gui_pixel_size * canvas.width + window_x / gui_pixel_size;
}

bool is_on_canvas(int window_x, int window_y)
{
    return window_x > 0 && window_x < canvas.width * gui_pixel_size && window_y > 0 && window_y < canvas.height * gui_pixel_size;
}

// Defining brush bindings
SDL_Keycode brush_bindings[TOTAL_BRUSHES] = {
    SDLK_b,
    SDLK_e,
    SDLK_s,
    SDLK_d,
    SDLK_l
};

struct
{
    SDL_Keycode key;
    mode_e mode;
} mode_bindings[TOTAL_MODES] = {
    {SDLK_v, MODE_VERTICAL_MIRROR},
    {SDLK_h, MODE_HORIZONTAL_MIRROR}
};

void handle_left_click(SDL_Event *event)
{
    // Check if the click happened inside the palette editor
    if (event->button.y > canvas.height * gui_pixel_size)
    {
        // Using a simple formula to calculate the index, remember that the list is visually wrapped
        size_t index = event->button.x / PALETTE_PIXEL_SIZE + (event->button.y - canvas.height * gui_pixel_size) / PALETTE_PIXEL_SIZE * palette_colors_per_row;

        if (index < canvas.palette_length)
        {
            canvas_set_active_color(&canvas, &canvas.palette[index]);
        }
 
        return;
    }

    size_t index = window_to_pixel_index(event->motion.x, event->motion.y);

    // Don't apply the brush if the mouse is not inside the selected region
    if (!canvas_is_index_inside_selection(&canvas, index)) return;
    
    canvas_create_new_history_entry(&canvas);
    canvas_apply_brush(&canvas, canvas.active_brush, index);

    return;
}

void handle_mouse_motion(SDL_Event *event)
{    
    // Check if mouse is outside the pixel grid
    // NOTE: This takes into consideration that the grid is docked in (0, 0) so I might want to rewrite this in the feature
    if (!is_on_canvas(event->motion.x, event->motion.y)) return;

    size_t index = window_to_pixel_index(event->motion.x, event->motion.y);
    
    // If the user hasn't yet moved to a new pixel, don't bother applying the tool
    // This is a great performance saver
    if (latest_pixel_index == index)
    {
        return;
    }

    latest_pixel_index = index;

    // Don't apply the brushes if the left mouse is not being held
    if (!is_holding_left_mouse) return;
    if (!canvas_is_index_inside_selection(&canvas, index)) return;

    drawing_while_holding_mouse = true;

    canvas_apply_brush(&canvas, canvas.active_brush, index);

    return;
}

void render_palette(void)
{   
    // The palette should wrap when it exceeds the window width
    for (int i = 0; i < canvas.palette_length; i++)
    {
        size_t x = i % palette_colors_per_row * PALETTE_PIXEL_SIZE;
        size_t y = canvas.height * gui_pixel_size + i / palette_colors_per_row * PALETTE_PIXEL_SIZE;
 
        SDL_SetRenderDrawColor(renderer, canvas.palette[i].r, canvas.palette[i].g, canvas.palette[i].b, 255);
        SDL_RenderFillRect(renderer, & (SDL_Rect) {x, y, PALETTE_PIXEL_SIZE, PALETTE_PIXEL_SIZE});
    }
}

void render_canvas_pixel(pixel_state_t *state, size_t i)
{
    canvas_color_t *color = &state->pixels[i];
    
    pixel.x = i % canvas.width * gui_pixel_size;
    pixel.y = i / canvas.width * gui_pixel_size;
  
    SDL_SetRenderDrawColor(renderer, color->r, color->g, color->b, 255);
    SDL_RenderFillRect(renderer, &pixel);
}

void calculate_window_size(int *width, int *height)
{
    *width = canvas.width * gui_pixel_size;
    *height = canvas.height * gui_pixel_size + (canvas.palette_length / (palette_colors_per_row + 1) + 1) * PALETTE_PIXEL_SIZE;
}

void update_pixel_size(unsigned int new_value)
{
    gui_pixel_size = new_value;
    palette_colors_per_row = canvas.width * gui_pixel_size / PALETTE_PIXEL_SIZE;

    pixel.w = pixel.h = gui_pixel_size;
}

void calculate_ideal_pixel_size(void)
{
    // Calculating the ideal pixel size based on how many pixels there are on the screen
    // I'm just playing around with some hard-coded values
    if (canvas.width > 300 || canvas.height > 300)
    {
        update_pixel_size(1);
    }
    else if (canvas.width > 200 || canvas.height > 200)
    {
        update_pixel_size(2);
    }
    else if (canvas.width > 100 || canvas.height > 100)
    {
        update_pixel_size(3);
    }
    else
    {
        // For every X amount of pixels on the screen, make the canvas pixel size smaller until you reach a minimum
        update_pixel_size(MAX(6, 25 - canvas.width * canvas.height / 100));
    }
}

void draw_canvas_selection(void)
{
    SDL_SetRenderDrawColor(renderer, 200, 100, 200, 255);
    SDL_RenderDrawRect(renderer, &selection_rect);
}

void clear_selection_visually(void)
{
    // Only re-rendering the pixels at the edge of the selection
    for (int i = 0; i < canvas.selection.width; i++)
    {
        render_canvas_pixel(&canvas.history[0], canvas.selection.top_left + i);
        render_canvas_pixel(&canvas.history[0], canvas.selection.top_left + (canvas.selection.height - 1) * canvas.width + i);
    }

    for (int i = 0; i < canvas.selection.height; i++)
    {
        render_canvas_pixel(&canvas.history[0], canvas.selection.top_left + i * canvas.width);
        render_canvas_pixel(&canvas.history[0], canvas.selection.top_left + (canvas.selection.width - 1) + canvas.width * i);
    }
}

void update_selection_visually(void)
{
    selection_rect.w = canvas.selection.width * gui_pixel_size;
    selection_rect.h = canvas.selection.height * gui_pixel_size;
    selection_rect.x = (canvas.selection.top_left % canvas.width) * gui_pixel_size;
    selection_rect.y = (canvas.selection.top_left / canvas.width) * gui_pixel_size;

    draw_canvas_selection();
    SDL_RenderPresent(renderer);
}

void redraw_entire_editor(void)
{
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);

    for (int i = 0; i < canvas.width * canvas.height; i++)
    {
        render_canvas_pixel(&canvas.history[0], i);
    }

    draw_canvas_selection();
    render_palette();
    SDL_RenderPresent(renderer);
}

void update_pixel_size_and_rerender(int new_pixel_size)
{
    update_pixel_size(new_pixel_size);
    update_selection_visually();
    int new_width, new_height;
    calculate_window_size(&new_width, &new_height);
    
    // Calculating width and height based on palette wrapping and pixel size
    SDL_SetWindowSize(window, new_width, new_height);

    redraw_entire_editor();
}

void handle_event(SDL_Event *event)
{
    drawing_while_holding_mouse = false;
    
    switch (event->type)
    {
    case SDL_MOUSEBUTTONDOWN:
        if (event->button.button != SDL_BUTTON_LEFT) return;
  
        is_holding_left_mouse = true;

        handle_left_click(event);
        return;

    case SDL_WINDOWEVENT:
        switch (event->window.event)
        {
            // When a region of the window suddenly appears into view again, we need to re-render the whole display
            // This can happen during window movement, minimization or maximazation, workspace switching etc.
        case SDL_WINDOWEVENT_EXPOSED:
            redraw_entire_editor();
            return;
        }

        return;
 
    case SDL_KEYDOWN:
        // Ignore the event it was a repeated key
        // We don't want there to be spam
        if (event->key.repeat > 0) return;
 
        for (int i = 0; i < TOTAL_MODES; i++)
        {
            if (event->key.keysym.sym == mode_bindings[i].key)
            {
                // They should toggle when their bindings are pressed (XOR)
                canvas.active_modes ^= mode_bindings[i].mode;
   
                return;
            }
        }

        // If we're out of the canvas, don't apply any drawing tools because we're going to get some undefined behaviour
        int mouse_x, mouse_y;
        SDL_GetMouseState(&mouse_x, &mouse_y);

        // For effects to work, we must be inside the canvas
        if (!is_on_canvas(mouse_x, mouse_y)) return;
        size_t index = window_to_pixel_index(mouse_x, mouse_y);
 
        // Don't apply non-brushes if the user is holding the mouse! It can cause bugs
        // Some fundamental editor bindings are hard-coded here
        if (!is_holding_left_mouse)
        {
            switch (event->key.keysym.sym)
            {
            case SDLK_w:
                canvas_save_progress(&canvas);
                return;

                // Clearing the selection area
            case SDLK_ESCAPE:
                clear_selection_visually();
                canvas_clear_selection(&canvas);
                update_selection_visually();
                draw_canvas_selection();
                SDL_RenderPresent(renderer);

                return;
  
            case SDLK_u:
                if (canvas.history_length == 1) return;
  
                // Undo is sepcial
                // We will only be re-rendering the pixels that are different from the previous state's corresponding color
                for (int i = 0; i < canvas.width * canvas.height; i++)
                {
                    if (!canvas.history[0].was_modified_in_this_state[i]) continue;
                    render_canvas_pixel(&canvas.history[1], i);
                }

                draw_canvas_selection();
                SDL_RenderPresent(renderer);
                canvas_delete_latest_history_entry(&canvas);

                return;
  
                // Modifies window size by adjusting individual pixel size
            case SDLK_MINUS:
                update_pixel_size_and_rerender(MAX(1, gui_pixel_size - 1));
                return;
  
            case SDLK_EQUALS:
                update_pixel_size_and_rerender(MIN(25, gui_pixel_size + 1));
                return;

                // Shape region selection
            case SDLK_1:
                canvas.shape_selection.start = index;
                canvas_update_selection_boundaries(&canvas, &canvas.shape_selection);
  
                return;

            case SDLK_2:
                canvas.shape_selection.end = index;
                canvas_update_selection_boundaries(&canvas, &canvas.shape_selection);

                return;

                // Canvas selection bindings
            case SDLK_9:
                clear_selection_visually();
  
                canvas.selection.start = index;
                canvas_update_selection_boundaries(&canvas, &canvas.selection);
                update_selection_visually();

                return;

            case SDLK_0:
                clear_selection_visually();
  
                canvas.selection.end = index;
                canvas_update_selection_boundaries(&canvas, &canvas.selection);
                update_selection_visually();
  
                return;
            }

            for (int i = 0; i < TOTAL_EFFECTS; i++)
            {
                if (event->key.keysym.sym == effect_bindings[i].key)
                {
                    // The yank effect clears the selection, so we need to get rid of the last one before proceeding
                    if (i == EFFECT_YANK)
                        clear_selection_visually();

                    // Most effects work with references to the previous state, this is really important
                    canvas_create_new_history_entry(&canvas);
                    effect_bindings[i].effect_handler(&canvas, latest_pixel_index);

                    switch (i)
                    {
                        // After the kill effect is applied, it is more efficient to just draw a rectangle on top of the deleted area
                    case EFFECT_KILL:
                        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
                        SDL_RenderFillRect(renderer, &selection_rect);
                        draw_canvas_selection();
                        SDL_RenderPresent(renderer);
   
                        return;
   
                    case EFFECT_YANK:
                        update_selection_visually();
                        return;
                    }

                    return;
                }
            }
        }

        // Brush selection
        for (int i = 0; i < TOTAL_BRUSHES; i++)
        {
            if (event->key.keysym.sym == brush_bindings[i])
            {
                canvas.active_brush = i;

                return;
            }
        }
   
        return;
  
    case SDL_MOUSEMOTION:
        handle_mouse_motion(event);
        return;

    case SDL_MOUSEBUTTONUP:
        if (event->button.button == SDL_BUTTON_LEFT)
        {
            is_holding_left_mouse = false;
        }

        return;
    }
}

void parse_cli_arguments(int argc, char **argv)
{
    if (argc < 3)
    {
        abort_with_help("please specify at least 2 arguments");
    }

    char *palette_path = argv[1];
    char *file_path = argv[2];

    // Width and height will be left as zero if they were not specified
    uintmax_t width, height = 0;

    // Parsing dimensions and checking for errors
    if (argc > 3)
    {
        errno = 0;
 
        width = strtoumax(argv[3], NULL, 10);
        if (!width || errno != 0) abort_with_help("could not parse width, i.e the second argument");
    }

    if (argc > 4)
    {
        height = strtoumax(argv[4], NULL, 10);
        if (!height || errno != 0) abort_with_help("could not parse height, i.e the third argument");
    }
    
    // Check if the file already exists, and if it does just load it
    if (access(file_path, F_OK) == F_OK)
    {
        canvas_load_png_with_preferred_size(&canvas, file_path, width, height);
    }
    else
    {
        // If the file does not exist, the user needs to pass the new file dimensions as arguments too
        if (!width || !height)
        {
            abort_with_help("you're creating a new file, please specify the dimensions");
        }

        canvas_start_with_new_file(&canvas, file_path, width, height);  
    }
 
    canvas_load_palette(&canvas, palette_path);
    canvas_finish_initialization(&canvas);
}

int main(int argc, char **argv)
{
    parse_cli_arguments(argc, argv);
    srand(time(NULL));
    
    SDL_Init(SDL_INIT_VIDEO);

    // Creating the window title based on the current file name
    char title[256];
    sprintf(title, "%s - Psyfidot Pixel Art Editor", canvas.file_path);

    calculate_ideal_pixel_size();
    int width, height;
    calculate_window_size(&width, &height);
    
    window = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_SHOWN);
    renderer = SDL_CreateRenderer(window, SDL_RENDERER_ACCELERATED, -1);

    // Initial rendering of the editor
    update_selection_visually();
    redraw_entire_editor();
    
    // The rendering loop will be optimized to save as much performance as possible
    for (;;)
    {
        // Mouse motion events do not work well with large delays
        // The condition is mandatory for maximum performance and great user experience
        SDL_Delay(drawing_while_holding_mouse ? 20 : 80);

        // The older version of this editor used to work with SDL_WaitEvent
        // However, the old approach was feeling laggy and it was slow at capturing mouse movement events
        // Using SDL_PumpEvents and SDL_PeepEvents however, allows the program to capture all mouse motion events just like SLD_PollEvent,
        // and to simultaneously reach SDL_WaitEvents' performance using a simple SDL_Delay statement
        SDL_PumpEvents();

        while (SDL_PeepEvents(&event, 1, SDL_GETEVENT, SDL_FIRSTEVENT, SDL_LASTEVENT))
        {
            if (event.type == SDL_QUIT) goto destroy_editor;
            handle_event(&event);
        }

        if (queue_is_empty(&canvas.undrawn_modifications)) continue;
 
    render_canvas:
        while (!queue_is_empty(&canvas.undrawn_modifications))
        {
            render_canvas_pixel(&canvas.history[0], queue_pop(&canvas.undrawn_modifications));
        }

        // Drawing the selection outline
        draw_canvas_selection();
 
        // The palette will be drawn on demand, only when that is absolutely necessary (i.e. when resizing the window)
        SDL_RenderPresent(renderer);
        queue_clear(&canvas.undrawn_modifications);
    }

destroy_editor:
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}
