## Psyfidot: a minimal, ~highly-optimized~ (check out psyfidot 2), keybinding-powered and lightweight pixel art editor with absolutely no visual clutter

Psyfidot is a simple pixel art editor with a variety of built-in tools, modes and effects. There's absolutely no visual clutter, except a tiny palette list at the bottom of the canvas.
You don't have to waste time moving your mouse around menus in Psyfidot. Here's a simple overview of the editor's philosophy:

![Screenshot of psyfidot with a painting of a blonde man](./screenshots/man.png)
![Screenshot of psyfidot with Pioyi](./screenshots/pioyi.png)

### Documentation

Psyfidot tooling is separated into three categories:
- `` Brushes ``: They apply whenever the user clicks or holds the left mouse button inside the selected region
- `` Effects ``: They only apply once when their key-bindings are pressed and they can manipulate the entire region at once
- `` Modes ``: They alter the behaviour of brushes and they can be toggled using their key-bindings. More that one can be enabled at the same time!

When you want to modify the dimensions of the current canvas, you currently have to save and re-execute the editor by passing the desired witdth and height as the second and third argument respectively. The image will be cropped if that is necessary, but nothing will be applied unless you execute the save command yourself!

With that out of the way, here are all key-bindings that one should memorize:
| Key Binding |  Description |
|:-----|:--------|
| `` n `` | Enables the normal brush |
| `` s `` | Enables the spray brush |
| `` e `` | Enables the eraser brush |
| `` d `` | Enables the darkening brush |
| `` l `` | Enables the lighting brush |
| `` f `` | Fills using the bucket effect and the current active color |
| `` o `` | Creates an outline around the non-empty pixels using the active color |
| `` i `` | Inverts all colors in the selected region |
| `` LEFT_ARROW `` | Moves all selected pixels one place to the left |
| `` RIGHT_ARROW `` | Moves all selected pixels one place to the right |
| `` UP_ARROW `` | Moves all selected pixels one place upwards |
| `` DOWN_ARROW `` | Moves all selected pixels one place downwards |
| `` ` `` | Applies the color picker or pipette: Replaces your active color with the color right under your cursor |
| `` a `` | Replace all instances of the color under the cursor inside the selected region with the active color |
| `` k `` | Kills (clears and saves into history) the whole selected region |
| `` y `` | Yanks (copies) the contents of the region to the clipboard |
| `` p `` | Pastes the contents of the clipboard right next to the cursor |
| `` ESC `` | Clears the selection (selects the whole canvas) |
| `` 1 `` | Sets the start of the shape selection using the cursor position (no GUI feedback) |
| `` 2 `` | Sets the end of the shape selection using the cursor position (no GUI feedback) |
| `` r `` | Draws a filled rectangle using the active color and the shape selection. If the selection is empty, it won't be executed |
| `` 4 `` | Draws the outline of a rectangle using the active color and the shape selection |
| `` 9 `` | Sets the start of the selection region using the cursor position |
| `` 0 `` | Updates the end of the selection region using the cursor position |
| `` h `` | Toggles horizontal axis mirror mode |
| `` v `` | Toggles vertical axis mirror mode |
| `` w `` | Saves the current file using the path that it was opened with |
| `` u `` | Undos changes and goes back to the previous state of the canvas' history |
| `` EQUALS `` | Makes each grid pixel appear bigger by making the window larger |
| `` MINUS `` | Makes each grid pixel appear smaller by shrinking the window |

To get detailed instructions on how to run the actual binary, just execute it with no arguments and a help message will appear to assist you.

### Contibuting

If you've spotted any bugs or want to make any sort of suggestion feel free to write an issue. I created this project because I couldn't find any other great alternatives out there. Libresprite was lagging and all the other editors were missing features that I couldn't live without. I would be very happy to implement your ideas!

~There are plans to move to an OpenGL frontend once I've actually learned how to do that :)~  
A complete and more efficient OpenGL rewrite is now available in my profile! :D