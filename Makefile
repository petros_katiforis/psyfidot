# Returns all c files nested or not in $(1)
define collect_sources
	$(shell find $(1) -name '*.c')
endef

SOURCES = $(call collect_sources, src)
OBJECTS = $(patsubst %.c, objects/%.o, $(SOURCES))

LD_FLAGS = `pkg-config --libs sdl2 libpng`

.PHONY: build
all: build

build: $(OBJECTS)
	@echo "[Makefile] Creating the executable"
	@$(CC) $^ -o bin $(LD_FLAGS)

	@./bin example-palette.psy example.png

objects/%.o: %.c
	@# Making sure that the directory already exists before creating the object
	@mkdir -p $(dir $@)

	@echo "[Makefile] Building $@"
	@$(CC) -c $< -o $@

